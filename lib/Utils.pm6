unit module Utils;

#|«
Check if two minterms differ by a single bit. Return both whether
they differ by a single bit and the 0-zero based mismatch index if they do.
»
sub compare( Str:D $mt1, Str:D $mt2 --> List ) is export {
    my Int:D $bit-diff = 0;
    my Int $mismatch-index; # where * < $mt1.chars;

    # compare minterms's bits and increment counter if they differ.
    for 0 ..^ $mt1.chars {
        if $mt1.comb[$_] ne $mt2.comb[$_] {
            $mismatch-index = $_;
            $bit-diff += 1;
        }
    }

    # True if only a single bit differs in both minterms...
    return (True, $mismatch-index) if $bit-diff == 1;

    # ... Otherwise, False.
    return (False, Nil);
}

#| Find the variable representation for a given minterm.
sub find-variables( Str:D $minterm --> List ) is export {
    =begin comment
    Find the variable representation for a given minterm. For instance,
    the minterm "1010" correspond to [A, B', C, D'].
    =end comment

    my @var-list;
    loop (my Int:D $i = 0; $i < $minterm.chars; $i++) {
        given $minterm.comb[$i] {
            # complemented variable
            when 0 { @var-list.push: ($i + 65).chr ~ "'" }

            # non-complemented variable
            when 1 { @var-list.push: ($i + 65).chr       }
        }
    }

    return @var-list.list;
}

#| Find the minterms that make up a combined minterm.
sub find-combined-minterms( Str:D $resultant --> List ) is export {
    my Int:D $gaps = $resultant.comb('-').Int;

    # resultant minterm isn't a combination of other minterms
    # so return its decimal representation.
    return List.new($resultant.parse-base(2).Str) if $gaps == 0;

    # binary representation of decimals from 0 up to 2 ^ $gaps
    my @bin = (^ 2**$gaps).map({ "%0*d".sprintf: $gaps, $_.base(2) });

    my @minterms = gather for 0 ..^ 2**$gaps {
        my ($temp, $idx) = $resultant, -1;

        # traverse each binary's bits
        for @bin.first.comb -> $j {
            $idx = $idx != -1
                 ?? $idx + $resultant.substr($idx+1..*).index('-') + 1
                 !! $resultant.substr($idx+1..*).index('-');
            $temp = $temp.substr(^$idx) ~ $j ~ $temp.substr($idx+1..*);
        }

        # pop binary from @bin's front
        @bin.shift;

        # grab the binary numeral's decimal representation
        take $temp.parse-base(2);
    }

    return @minterms.list;
}

#| Multiply two minterms in their variable representation.
sub mul( @m1, @m2 --> List ) is export {
    my @res;

    for @m1 -> $literal {
        given $literal {
            # when literal is complemented and its non-complemented form
            # is in @m2
            when .contains("'") and .substr(0,1) ∈ @m2 { return [] }

            # when literal's complemented form is in @m2
            when ($_, "'").join ∈ @m2 { return [] }

            default { @res.push: $literal }
        }
    }

    # get the remaining literals from @m2 that aren't in @res yet
    for @m2 -> $literal {
        @res.push: $literal if $literal ∉ @res;
    }

    return @res;
}

#| Multiply all the prime implicants that cover two minterms.
sub multiply( @m1, @m2 --> List ) is export {
    my @res;

    for @m1 X @m2 {
        my @tmp = mul($_.first, $_.tail);
        @res.push: @tmp if @tmp.elems != 0;
    }

    return @res;
}

#|«
Remove the essential prime implicants (EPIs) from the prime implicants chart
and return only the minterms that weren't covered by the EPIs.
»
sub remove-terms( @epi, %chart --> Hash:D ) is export {
    for @epi -> $term {
        for find-combined-minterms($term) -> $minterm {
            # remove minterm since it's covered by an EPI.
            %chart{$minterm}:delete;
        }
    }

    return %chart;
}
